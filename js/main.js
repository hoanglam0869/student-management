var studentList = [];
const STUDENT_LIST_LOCAL = "STUDENT_LIST_LOCAL";

var jsonData = localStorage.getItem(STUDENT_LIST_LOCAL);
if (jsonData != null) {
  studentList = JSON.parse(jsonData).map(function (student) {
    return new Student(
      student.number,
      student.name,
      student.email,
      student.password,
      student.math,
      student.physics,
      student.chemistry
    );
  });
  renderTable(studentList);
}

function addStudent() {
  var student = getFormData();
  studentList.push(student);
  renderTable(studentList);
  saveData();
}

function saveData() {
  var dataJSON = JSON.stringify(studentList);
  localStorage.setItem(STUDENT_LIST_LOCAL, dataJSON);
}

function removeStudent(number) {
  var index = studentList.findIndex(function (student) {
    return student.number == number;
  });
  if (index != -1) {
    studentList.splice(index, 1);
    renderTable(studentList);
    saveData();
  }
}

function editStudent(number) {
  var index = studentList.findIndex(function (student) {
    return student.number == number;
  });
  var student = studentList[index];
  document.getElementById("txtMaSV").value = student.number;
  document.getElementById("txtTenSV").value = student.name;
  document.getElementById("txtEmail").value = student.email;
  document.getElementById("txtPass").value = student.password;
  document.getElementById("txtDiemToan").value = student.math;
  document.getElementById("txtDiemLy").value = student.physics;
  document.getElementById("txtDiemHoa").value = student.chemistry;
}

function updateStudent() {
  var student = getFormData();
  var index = studentList.findIndex(function (item) {
    return item.number == student.number;
  });
  studentList[index] = student;
  renderTable(studentList);
  saveData();
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

function searchStudent() {
  var name = document.getElementById("txtSearch").value;
  var filterList = studentList.filter(function (student) {
    return student.name.toUpperCase().indexOf(name.toUpperCase()) > -1;
  });
  renderTable(filterList);
}

document.getElementById("txtSearch").onkeyup = function (e) {
  if (e.key == "Enter") {
    searchStudent();
  }
};
