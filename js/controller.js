function getFormData() {
  var number = document.getElementById("txtMaSV").value;
  var name = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var math = document.getElementById("txtDiemToan").value * 1;
  var physics = document.getElementById("txtDiemLy").value * 1;
  var chemistry = document.getElementById("txtDiemHoa").value * 1;

  return new Student(number, name, email, password, math, physics, chemistry);
}

function renderTable(studentList) {
  document.getElementById("tbodySinhVien").innerHTML = studentList
    .map(function (student) {
      return `<tr>
            <td>${student.number}</td>
            <td>${student.name}</td>
            <td>${student.email}</td>
            <td></td>
            <td></td>
            <td>${Math.round(student.average(), 2)}</td>
            <td>
                <button onclick="removeStudent('${
                  student.number
                }')" class="btn btn-danger">Xóa</button>
                <button onclick="editStudent('${
                  student.number
                }')" class="btn btn-warning">Sửa</button>
            </td>
        </tr>`;
    })
    .join("");
}
